Petit fichier regroupant un ensemble de fonctions permettants des calculs d'aires simples de formes géométriques.
Voici comment utiliser ces fonctions.

Carré : 
`carre(cote)`

Rectangle : 
`rectangle(longueur, largeur)`

Triangle :
`triangle(base, hauteur)`

Disque : 
`disque(rayon)`

Losange :
`losange(grandeDiagonale, petiteDiagonale)`

Trapère :
`trapeze(petiteBase, grandeBase, hauteur)`

Parallélogramme : 
`parallelogramme(base, hauteur)`

Pentagone Régulier :
`pentagoneRegulier(base, apotheme)`

Couronne (ou anneau) :
`couronne(petitRayon, grandRayon)`

Demi-cercle :
`demiCercle(rayon)`

Ellipse :
`ellipse(grandAxe, petitAxe)`

Hexagone Régulier :
`hexagoneRegulier(cote)`