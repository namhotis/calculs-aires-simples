function carre(cote) {
    var aireCarre = cote*cote;
    return aireCarre;
}

function rectangle(longueur, largeur) {
    var aireRectangle = longueur*largeur;
    return aireRectangle;
}

function triangle(base, hauteur) {
    var aireTriangle = (base*hauteur)/2;
    return aireTriangle;
}

function disque(rayon) {
    var aireDisque = Math.PI*rayon*rayon;
    return aireDisque;
}

function losange(grandeDiagonale, petiteDiagonale) {
    var aireLosange = (grandeDiagonale * petiteDiagonale)/2;
    return aireLosange;
}

function trapeze(petiteBase, grandeBase, hauteur) {
    var aireTrapeze = ((petiteBase + grandeBase) * hauteur)/2;
    return aireTrapeze;
}

function parallelogramme(base, hauteur) {
    var aireParallelogramme = base * hauteur;
    return aireParallelogramme;
}

function pentagoneRegulier(base, apotheme) {
    var airePentagoneRegulier = 5 * ((base * apotheme) / 2);
    return airePentagoneRegulier;
}

function couronne(petitRayon, grandRayon) {
    var aireCouronne = Math.PI * ((grandRayon * grandRayon) - (petitRayon * petitRayon));
    return aireCouronne;
}

function demiCercle(rayon) {
    var aireDemiCercle = 0.5 * Math.PI * (rayon * rayon);
    return aireDemiCercle;
}

function ellipse(grandAxe, petitAxe) {
    var aireEllipse = Math.PI * grandAxe * petitAxe;
    return aireEllipse;
}

function hexagoneRegulier(cote) {
    var aireHexagoneRegulier = ((3*Math.sqrt(3))/2)*(cote*cote);
    return aireHexagoneRegulier;
}